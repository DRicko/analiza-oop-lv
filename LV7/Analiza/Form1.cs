﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza
{
    public partial class MainForm : Form
    {
        public class TicTacToe
        {
            public TextBox player1, player2;
            public Label player1Score, player2Score;
            private int player1S= 0, player2S= 0;
            public Label turnIndicator;
            public List<Button> Board;
            private int firstPlayer = 2;
            private int playersTurn = 1;
            private int turnsLeft;
            public Button startButton;
            
            private void GameOver(bool win)
            {
                if (!win)
                {
                    MessageBox.Show("Tied match.", "Info");
                }

                foreach (Button B in Board)
                {
                    B.Enabled = false;
                }
                player1.Enabled = true;
                player2.Enabled = true;
                startButton.Enabled = true;
            }

            private void Victory(string Victor)
            {
                if(Victor == "X")
                {
                    player1S++;   
                    MessageBox.Show("Player " + player1.Text + " wins!");
                }
                else
                {
                    player2S++;
                    MessageBox.Show("Player " + player2.Text + " wins!");
                }
                player1Score.Text = player1S.ToString();
                player2Score.Text = player2S.ToString();
                GameOver(true);
            }

            private void CheckBoard()
            {
                String S;
                for (int i = 0, j = 0; i < 3; i++)
                {
                    S = null;
                    if (Board[3 * i + i].Text != "")
                    {
                        S = Board[3 * i + i].Text;
                    }
                    else continue;
                    
                    //Dolje,Gore
                    for(j = 0; j < 3; j++)
                    {
                        if(S != Board[3*j + i].Text)
                        {
                            j = -1;
                            break;
                        }
                    }
                    if (j == 3) { Victory(S);return; }

                    //Lijevo,Desno
                    for (j = 0; j < 3; j++)
                    {
                        if (S != Board[3 * i + j].Text)
                        {
                            j = -1;
                            break;
                        }
                    }
                    if (j == 3) { Victory(S); return; }                 
                }
                S = Board[4].Text;
                if (S != "")
                {
                    if ((Board[0].Text == S && S == Board[8].Text) || (Board[6].Text == S && S == Board[2].Text))
                    {
                        Victory(S); return;
                    }
                }

                if (turnsLeft <= 0)
                {
                    GameOver(false);
                }
            }

            public void NewTurn(object sender)
            {
                Button S = null;
                foreach(Button B in Board)
                {
                    if (sender.GetHashCode() == B.GetHashCode())
                    {
                        S = B; break;
                    }
                }

                if (S == null) return;

                S.Enabled = false;

                switch (playersTurn)
                {
                    case 1:
                        S.Text = "X";
                        playersTurn = 2;
                        turnIndicator.Text = "Turn: " + player2.Text;
                        break;
                    case 2:
                        S.Text = "O";
                        playersTurn = 1;
                        turnIndicator.Text = "Turn: " + player1.Text;
                        break;
                }
                turnsLeft--;
                CheckBoard();
            }

            private void NewRound()
            {
                foreach (Button B in Board)
                {
                    B.Enabled = true;
                    B.Text = "";
                }
                firstPlayer = ((firstPlayer == 1) ? (2) : (1));
                playersTurn = firstPlayer;
                turnIndicator.Text = "Turn: " + ((playersTurn == 1) ? (player1.Text) : (player2.Text));
                turnsLeft = 9;
            }

            public void Start()
            {
                
                if (player1.Text == "" || player2.Text == "")
                {
                    MessageBox.Show("Player names cannot be empty.", "Error");
                    return;
                }
                if(player1.Text == player2.Text)
                {
                    MessageBox.Show("Players cannot have the same name.", "Error");
                    return;
                }

                player1.Enabled = false;
                player2.Enabled = false;
                NewRound();
                startButton.Enabled = false;
            }
        }
        TicTacToe Game = new TicTacToe();
        List<Button> BL = new List<Button>();
        public MainForm()
        {
            InitializeComponent();
            BL.Add(button1);
            BL.Add(button2);
            BL.Add(button3);
            BL.Add(button4);
            BL.Add(button5);
            BL.Add(button6);
            BL.Add(button7);
            BL.Add(button8);
            BL.Add(button9);

            foreach(Button B in BL)
            {
                B.Click += new System.EventHandler(this.ChooseSPot);
            }

            Game.turnIndicator = PlayerTurnLabel;
            Game.player1 = Player1Name;
            Game.player2 = Player2Name;
            Game.Board = BL;
            Game.startButton = StartButton;
            Game.player1Score = Player1ScoreLabel;
            Game.player2Score = Player2ScoreLabel;
        }

        private void ChooseSPot(object sender, EventArgs e)
        {
            Game.NewTurn(sender);
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            Game.Start();
        }
    }
}
