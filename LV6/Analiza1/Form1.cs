using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace DesktopApp1
{

    public partial class Calculator : Form
    {
        public class CalculatorBase
        {
            private double valueA = 0, valueB = 0;
            private Boolean valueAUsed  = false;
            private int decimals        = 1;
            private int operation       = 0;
            private Boolean decimalUsed = false;
            public string Operation    = "";
            public string ValueA       = "";
            public string ValueB       = "";
            private Boolean cleanAfterResult = false;
            private Boolean negativeA = false, negativeB = false;

            private void Operate(int n)
            { 
                if (valueAUsed && n != 5 && n == operation)
                {
                    Equal();
                }
                else
                {
                    valueAUsed = true;
                }
                operation = n;
                cleanAfterResult = false;
                decimalUsed = false;
                decimals = 1;
                negativeB = false;
                negativeA = (valueA < 0) ? (true) : (false);
                SetDisplay();
                switch (operation)
                {
                    case 0: Operation = "+"; break;
                    case 1: Operation = "-"; break;
                    case 2: Operation = "*"; break;
                    case 3: Operation = "/"; break;
                    case 4: Operation = "^"; break;
                    case 5: Operation = "log"; break;
                }
                
            }

            public void Sine()
            {
                Equal();
                valueA = Math.Sin(valueA);
                SetDisplay();
            }

            public void Cosine()
            {
                Equal();
                valueA = Math.Cos(valueA);
                SetDisplay();
            }

            public void Log()
            {
                Operate(5);
                SetDisplay();
            }

            public void Pow()
            {
                Operate(4);
            }

            public void Sqrt()
            {
                Equal();
                valueA = Math.Sqrt(valueA);
                SetDisplay();
                valueAUsed = true;
            }

            public void Clear()
            {
                valueA = 0; valueB = 0;
                valueAUsed = false;
                decimals = 1;
                operation = 0;
                decimalUsed = false;
                Operation = "";
                cleanAfterResult = false;
                SetDisplay();
            }

            public void OperationDivide()
            {
                Operate(3);
            }

            public void OperationMultiply()
            {
                Operate(2);
            }

            public void OperationRetract()
            {
                if(operation < 2)
                {
                    Operate(1);
                }
                else
                {
                    if (valueAUsed)
                    {
                        negativeB = !negativeB;
                    }
                    else
                    {
                        negativeA = !negativeA;
                    }
                }
            }

            public void OperationSum()
            {
                Operate(0);
            }

            public void Decimal()
            {
                decimalUsed = true;
            }

            public void Equal()
            {
                switch (operation)
                {
                    case 0:
                        if (negativeA)
                        {
                            Boolean Greater = false;
                            if (valueA > valueB) Greater = true;
                            valueA += (negativeB) ? (valueB) : (-valueB);
                            if (Greater)
                            {
                                valueA *= -1;
                            }
                        }
                        else
                        {
                            valueA += (negativeB) ? (-valueB) : (valueB);
                        }
                        Operation = "+";
                        break;
                    case 1: valueA -= (negativeB) ? (-valueB) : (valueB); Operation = "-"; break;
                    case 2: valueA *= (negativeB) ? (-valueB) : (valueB); Operation = "*"; break;
                    case 3: valueA /= (negativeB) ? (-valueB) : (valueB); Operation = "/"; break;
                    case 4: valueA = Math.Pow(valueA, (negativeB) ? (-valueB) : (valueB)); Operation = "^"; break;
                    case 5: valueA = Math.Log((negativeB) ? (-valueB) : (valueB), valueA);
                        Operation = "";
                        operation = 0 ;
                        cleanAfterResult = true;
                        break;
                }
                valueB = 0;
                decimals = 1;
                decimalUsed = false;
                negativeB = false;
                negativeA = (valueA < 0) ? (true) : (false);
                valueA = (negativeA) ? (-valueA) : (valueA);
                SetDisplay();
            }

            public void InsertNumber(int num)
            {
                if (cleanAfterResult)
                {
                    cleanAfterResult = false;
                    valueA = 0;
                    valueAUsed = false;
                }

                if (valueAUsed)
                {
                    if (decimalUsed)
                    {
                        valueB = valueB + ((double)num * Math.Pow(10, -decimals));
                        decimals++;
                    }
                    else
                    {
                        valueB *= 10;
                        valueB += num;
                    }
                }
                else
                {
                    if (decimalUsed)
                    {
                        valueA += (double)num / Math.Pow(10, decimals);
                        decimals++;
                    }
                    else
                    {
                        valueA *= 10;
                        valueA += num;
                    }
                }
                SetDisplay();
            }
            private void SetDisplay()
            {
                ValueA = valueA.ToString();
                ValueB = valueB.ToString();
            }

        }
        CalculatorBase Calc = new CalculatorBase();
        public Calculator()
        {
            InitializeComponent();
        }
        private void SyncDisplay()
        {
            if (Calc.Operation == "")
            {
                Display.Text = Calc.ValueA;
            }
            else if(Calc.Operation == "log")
            {
                Display.Text = Calc.Operation + "(Base: " + Calc.ValueA + ", " + Calc.ValueB;
            }
            else
            {
                Display.Text = Calc.ValueA + " " + Calc.Operation + " " + Calc.ValueB;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(7);
            SyncDisplay();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(8);
            SyncDisplay();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(9);
            SyncDisplay();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(4);
            SyncDisplay();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(5);
            SyncDisplay();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(6);
            SyncDisplay();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(1);
            SyncDisplay();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(2);
            SyncDisplay();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(3);
            SyncDisplay();
        }

        private void buttonDecimal_Click(object sender, EventArgs e)
        {
            Calc.Decimal();
            SyncDisplay();
        }

        private void button0_Click(object sender, EventArgs e)
        {
            Calc.InsertNumber(0);
            SyncDisplay();
        }

        private void buttonEquals_Click(object sender, EventArgs e)
        {
            Calc.Equal();
            SyncDisplay();
        }

        private void buttonSum_Click(object sender, EventArgs e)
        {
            Calc.OperationSum();
            SyncDisplay();
        }

        private void buttonRetract_Click(object sender, EventArgs e)
        {
            Calc.OperationRetract();
            SyncDisplay();
        }

        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            Calc.OperationMultiply();
            SyncDisplay();
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            Calc.OperationDivide();
            SyncDisplay();
        }

        private void buttonSine_Click(object sender, EventArgs e)
        {
            Calc.Sine();
            SyncDisplay();
        }

        private void buttonCosine_Click(object sender, EventArgs e)
        {
            Calc.Cosine();
            SyncDisplay();
        }

        private void buttonLog_Click(object sender, EventArgs e)
        {
            Calc.Log();
            SyncDisplay();
        }

        private void buttonSqrt_Click(object sender, EventArgs e)
        {
            Calc.Sqrt();
            SyncDisplay();
        }

        private void buttonPow_Click(object sender, EventArgs e)
        {
            Calc.Pow();
            SyncDisplay();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            Calc.Clear();
            SyncDisplay();
        }
    }
}
