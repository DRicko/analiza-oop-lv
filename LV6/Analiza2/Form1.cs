﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza2
{
    public partial class MainForm : Form
    {
        public string @path = "dictionary.txt";
        
        public class Hangman
        {
            public List<string> Dictonary = new List<string>();
            public string currentWord;
            public string Output;
            public List<char> Correct = new List<char>();
            public List<char> Incorrect = new List<char>();
            public int ChanceCount = 6;
            public Boolean gameStarted = false;
            public Boolean gameOver = false;
            public Boolean victory = false;
            private static Random rnd = new Random();
            public void Init()
            {
                
                int r = rnd.Next(Dictonary.Count);
                currentWord = Dictonary[r];
                Correct.Clear();
                Incorrect.Clear();
                ChanceCount = 6;
                gameOver = false;
                victory = false;
                ModifyOutput();
            }

            private void GameOver()
            {

                gameOver = true;
                gameStarted = false;
            }

            private void ModifyOutput()
            {
                Output = "";
                int uncoveredLetters = 0;
                for (int i = 0; i < currentWord.Length; i++)
                {
                    Boolean found = false;
                    foreach(char s in Correct)
                    {
                        if(s == currentWord.ToLower()[i])
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                    {
                        Output += currentWord[i] + " ";
                    }
                    else
                    {
                        Output += "_ ";
                        uncoveredLetters++;
                    }
                }
                if (uncoveredLetters == 0)
                {
                    Victory();
                }
            }

            private void Victory()
            {
                victory = true;
                gameStarted = false;
            }
            private void AddToCorrect(char c)
            {
                Correct.Add(c);
                ModifyOutput();
            }
            private void AddToIncorrect(char c)
            {
                Incorrect.Add(c);
                ChanceCount--;
                if (ChanceCount == 0)
                {
                    GameOver();
                }
            }

            public void tryExpression(string s)
            {
                if (s == "") return;
                if (s.ToLower() == currentWord.ToLower())
                {
                    Victory();
                }
                else
                {
                    int i;
                    for (i = 0; i < s.Length && (s[i] == ' ' || s[i] == '\t'); i++) ;
                    int n = i;
                    for (i++; i < s.Length; i++)
                    {
                        if(!(s[i] == ' ' || s[i] == '\t'))
                        {
                            i = -1;
                            break;
                        }
                    }
                    if (i == -1)
                    {
                        GameOver();
                    }
                    else
                    {
                        for (i = 0; i < currentWord.Length; i++)
                        {
                            if (currentWord.ToLower()[i] == s[n])
                            {
                                AddToCorrect(s[n]);
                                i = -1;
                                break;    
                            }
                        }
                        if (i != -1)
                        {
                            AddToIncorrect(s[n]);
                        }
                    }

                }
            }

        }
        Hangman Game1 = new Hangman();
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                    {
                        string D = line.ToString(); // novi objekt
                        Game1.Dictonary.Add(D); // umetanje objekta u listu
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                MessageBox.Show("Datoteka " + @path + " nije pronađena.","Error");
            }
            catch
            {
                MessageBox.Show("Nepoznata pogreška", "Error");
                Application.Exit();
            }

        }

        private void SyncOutput()
        {
            Display.Text = Game1.Output;
            tryDisplay.Text = "Pokušaji: " + Game1.ChanceCount.ToString();
            usedLetters.Text = "Iskorištena slova: ";
            foreach (char c in Game1.Incorrect)
            {
                usedLetters.Text += c + " ";
            }

            if (Game1.victory)
            {
                MessageBox.Show("Victory", "Note");
            }
            if (Game1.gameOver)
            { 
                MessageBox.Show("Game Over", "Note");
                Display.Text += "\n" + Game1.currentWord;
            }
            
        }

        private void testExpression()
        {
            if (Game1.gameStarted)
            {
                Game1.tryExpression(tryText.Text);

                if (!Game1.gameStarted)
                {
                    tryButton.Text = "Započni";
                }
            }
            else
            {
                Game1.gameStarted = true;
                Game1.Init();
                tryButton.Text = "Pokušaj";
                Display.Text = Game1.Output;
                MessageBox.Show(Game1.currentWord, "Tip");
            }
            tryText.Text = "";
            SyncOutput();
        }

        private void tryButton_Click(object sender, EventArgs e)
        {
            testExpression();
        }
    }
}
