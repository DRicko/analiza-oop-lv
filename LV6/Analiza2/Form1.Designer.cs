﻿namespace Analiza2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tryText = new System.Windows.Forms.TextBox();
            this.Display = new System.Windows.Forms.Label();
            this.tryButton = new System.Windows.Forms.Button();
            this.tryDisplay = new System.Windows.Forms.Label();
            this.usedLetters = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tryText
            // 
            this.tryText.Location = new System.Drawing.Point(12, 124);
            this.tryText.Name = "tryText";
            this.tryText.Size = new System.Drawing.Size(677, 22);
            this.tryText.TabIndex = 0;
            // 
            // Display
            // 
            this.Display.AutoSize = true;
            this.Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Display.Location = new System.Drawing.Point(12, 9);
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(36, 39);
            this.Display.TabIndex = 1;
            this.Display.Text = "_";
            // 
            // tryButton
            // 
            this.tryButton.Location = new System.Drawing.Point(695, 101);
            this.tryButton.Name = "tryButton";
            this.tryButton.Size = new System.Drawing.Size(93, 45);
            this.tryButton.TabIndex = 2;
            this.tryButton.Text = "Započni";
            this.tryButton.UseVisualStyleBackColor = true;
            this.tryButton.Click += new System.EventHandler(this.tryButton_Click);
            // 
            // tryDisplay
            // 
            this.tryDisplay.AutoSize = true;
            this.tryDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tryDisplay.Location = new System.Drawing.Point(652, 9);
            this.tryDisplay.Name = "tryDisplay";
            this.tryDisplay.Size = new System.Drawing.Size(136, 29);
            this.tryDisplay.TabIndex = 3;
            this.tryDisplay.Text = "6 pokušaja";
            // 
            // usedLetters
            // 
            this.usedLetters.AutoSize = true;
            this.usedLetters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usedLetters.Location = new System.Drawing.Point(12, 101);
            this.usedLetters.Name = "usedLetters";
            this.usedLetters.Size = new System.Drawing.Size(144, 20);
            this.usedLetters.TabIndex = 4;
            this.usedLetters.Text = "Iskorištena slova: ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 158);
            this.Controls.Add(this.usedLetters);
            this.Controls.Add(this.tryDisplay);
            this.Controls.Add(this.tryButton);
            this.Controls.Add(this.Display);
            this.Controls.Add(this.tryText);
            this.Name = "MainForm";
            this.Text = "Vješala";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tryText;
        private System.Windows.Forms.Label Display;
        private System.Windows.Forms.Button tryButton;
        private System.Windows.Forms.Label tryDisplay;
        private System.Windows.Forms.Label usedLetters;
    }
}

